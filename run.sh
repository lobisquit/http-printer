#!/bin/bash
source venv/bin/activate
source .env
gunicorn --bind 0.0.0.0:5000 --timeout 90 wsgi:app
