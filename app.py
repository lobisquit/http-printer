import io
import logging
import os
import subprocess

from flask import Flask, flash, redirect, render_template, request, send_file
from flask_simplelogin import SimpleLogin, login_required
from werkzeug.security import check_password_hash

app = Flask(__name__)

# read parameters, fail if missing
app.config['SECRET_KEY'] = os.environ['SECRET_KEY']

SCANNER_DEVICE = os.environ['SCANNER_DEVICE']
PRINTER_DEVICE = os.environ['PRINTER_DEVICE']
HASHED_USER_PASSWORD = os.environ['HASHED_USER_PASSWORD']

# optional additional parameters
PRINTER_IP = os.environ.get('PRINTER_IP', None)

# setup logger
logger = logging.getLogger("scan")
LOGLEVEL = os.environ.get('LOGLEVEL', 'INFO').upper()
logger.setLevel(level=LOGLEVEL)


# setup the login manager
def login_checker(user):
    if user.get('username') == 'user' and \
       check_password_hash(HASHED_USER_PASSWORD, user.get('password')):
        return True
    return False


SimpleLogin(app, login_checker=login_checker)


@app.route("/")
@login_required
def main():
    active = True

    # assess activity if IP is provided
    if PRINTER_IP:
        # scan
        command = ["curl", "-I", PRINTER_IP]
        command = subprocess.run(command, capture_output=True)

        active = (command.returncode == 0)

    return render_template('home.html', active=active)


class ScannerError(Exception):
    pass


def _scanner(request, modes, resolutions):
    # parse mode
    mode = request.form.get('mode', None)

    if mode is None:
        raise ScannerError("Mode not selected")

    if mode not in modes:
        raise ScannerError(
            "Inserted mode '{}' is not among the valid ones: {}"
            .format(mode, ', '.join(modes)))

    # parse resolution
    resolution = request.form.get('resolution', None)

    if resolution is None:
        raise ScannerError("Resolution not selected")

    if resolution not in resolutions:
        raise ScannerError(
            "Inserted resolution '{}' is not among the valid ones: {}"
            .format(resolution, ', '.join(resolutions)))

    # scan
    command = ["scanimage",
               "--device-name", SCANNER_DEVICE,
               "--mode", mode,
               "--resolution", resolution,
               "--format", "jpeg"]

    command = subprocess.run(command, capture_output=True)

    if command.stderr:
        stderr = command.stderr.decode("utf-8").replace('\n', '')
        raise ScannerError(f"Error in scan command: {stderr}")

    return send_file(
        io.BytesIO(command.stdout),
        mimetype='image/jpeg',
        as_attachment=True,
        download_name='scan.jpeg')


@app.route("/scanner", methods=['GET', 'POST'])
@login_required
def scanner():
    MODES = ['Gray', 'Color', 'Lineart']
    RESOLUTIONS = ['150', '300', '1200']

    outcome = {'err': None}
    if request.method == 'POST':
        try:
            return _scanner(request,
                            modes=MODES,
                            resolutions=RESOLUTIONS)
        except ScannerError as ex:
            outcome['err'] = ex

    return render_template('scanner.html',
                           outcome=outcome,
                           modes=MODES,
                           resolutions=RESOLUTIONS)


@app.route("/printer", methods=['GET', 'POST'])
@login_required
def printer():
    outcome = {'ok': None, 'err': None}

    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)

        file = request.files['file']

        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)

        if file and file.filename.endswith('.pdf'):
            # send the print command to CUPS
            command = ["/usr/bin/lp", "-d", PRINTER_DEVICE]
            command = subprocess.run(command,
                                     capture_output=True,
                                     stdin=file.stream)

            if command.stderr:
                stderr = command.stderr.decode("utf-8").replace('\n', '')
                msg = f"Error in print command: {stderr}"
                logger.error(msg)
                outcome['err'] = msg
            else:
                stdout = command.stderr.decode("utf-8").replace('\n', '')

                msg = "Print job enqueued"
                if stdout:
                    msg = f"{msg}: {stdout}"

                logger.info(msg)
                outcome['ok'] = msg

    return render_template('printer.html', outcome=outcome)


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=5001)
